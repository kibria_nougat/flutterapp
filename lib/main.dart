import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {

  @override
  MyAppState createState() => new MyAppState();
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('LoveMeter'),
      ),
      body: new Center(
        child: new Container(
          padding: new EdgeInsets.all(32.0),
          child: new Center(

            child: new Column(
              children: <Widget>[

                new TextField(
                  decoration: new InputDecoration(hintText: 'Your Name'),
                ),
                new TextField(
                  decoration: new InputDecoration(
                      hintText: 'Name of Your Partner'),
                ),
                new Container(
                  child: new Center(
                      child: new MaterialButton(
                        onPressed: () {},
                        child: new Text('WEIGH YOUR LOVE'),
                        textColor: Colors.white,
                        color: Colors.red,
                      )
                  ),
                  margin: new EdgeInsets.all(20.0),
                )

              ],
            ),
          ),
        ),
      ),
    );
  }

}